import java.util.ArrayList;
import java.util.Random;

/**
 * Klasse zum Erstellen und Verwalten von Raumschiffen
 * 
 * @author Cyril Agostini
*/

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<>();
	private ArrayList<Ladung> ladungverzeichnis;

	/**
	 * Erstellung einer neuen Instanz und Variablen einrichten
	 */
	public Raumschiff() {
		setSchiffsname("Schiff1");
		setPhotonentorpedoAnzahl(5);
		setEnergieversorgungInProzent(100);
		setSchildeInProzent(100);
		setHuelleInProzent(100);
		setLebenserhaltungssystemeInProzent(100);
		setAndroidenAnzahl(3);
		
		this.ladungverzeichnis = new ArrayList<>();

	}
	/**
	 * Erstellung einer neuen Instanz und Variablen einrichten
	 * 
	 * 
	 * @param schiffsname Schiffsname
	 * @param photonentorpedoAnzahl Photonentorpedos Anzahl
	 * @param energieversorgungInProzent Energieversorgung in Prozent
	 * @param schildeInProzent Shutzschilde in Prozent
	 * @param huelleInProzent H�lle in Prozent
	 * @param lebenserhaltungssystemeInProzent Lebenserhaltungssysteme in Prozent
	 * @param androidenAnzahl Androiden Anzahl
	 */
	public Raumschiff(String schiffsname, int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl) 
	{
		setSchiffsname(schiffsname);
		setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
		setEnergieversorgungInProzent(energieversorgungInProzent);
		setSchildeInProzent(schildeInProzent);
		setHuelleInProzent(huelleInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setAndroidenAnzahl(androidenAnzahl);
		
		this.ladungverzeichnis = new ArrayList<>();

	}
	
	/**
	 * Get die Anzahl der photonentorpedosAnzahl
	 * 
	 * @return photonentorpedoAnzahl
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	/**
	 * die Anzahl der photonentorpedosAnzahl setzen
	 * 
	 * @param photonentorpedoAnzahl Photonentorpedos Anzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		if(photonentorpedoAnzahl < 0) photonentorpedoAnzahl = 0;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	/**
	 * Get die Energieversorgung in Prozent
	 * 
	 * @return energieversorgungInProzent 
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * die Energieversorgung in Prozent setzen
	 *  
	 * @param energieversorgungInProzent Energieversorgung in Prozent
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		if(energieversorgungInProzent < 0) energieversorgungInProzent = 0;
		if(energieversorgungInProzent > 100) energieversorgungInProzent = 100;
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	/**
	 * Get die Schilde in Prozent
	 * 
	 * @return schildeInProzent
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	/**
	 * Schilde in Prozent setzen
	 * 
	 * @param schildeInProzent Shutzschilde in Prozent
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		if(schildeInProzent < 0) schildeInProzent = 0;
		if(schildeInProzent > 100) schildeInProzent = 100;
		this.schildeInProzent = schildeInProzent;
	}
    /**
     * Get die H�lle in Prozent
     * 
     * @return huelleInProzent
     */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
    /**
     * H�lle in Prozent setzen
     * 
     * @param huelleInProzent H�lle in Prozent
     */
	public void setHuelleInProzent(int huelleInProzent) {
		if(huelleInProzent < 0) huelleInProzent = 0;
		if(huelleInProzent > 100) huelleInProzent = 100;
		this.huelleInProzent = huelleInProzent;
	}
	/**
	 * Get die Lebenserhaltungssysteme in Prozent
	 * 
	 * @return lebenserhaltungssystemeInProzent
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
    /**
     * Lebenserhaltungssysteme in Prozent setzen
     * 
     * @param lebenserhaltungssystemeInProzent Lebenserhaltungssysteme in Prozent
     */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		if(lebenserhaltungssystemeInProzent < 0) lebenserhaltungssystemeInProzent = 0;
		if(lebenserhaltungssystemeInProzent > 100) lebenserhaltungssystemeInProzent = 100;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	/**
	 * Get der Anzahl der Androiden 
	 * 
	 * @return androidenAnzahl 
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	/**
	 * Anzahl der Androiden setzen
	 * 
	 * @param androidenAnzahl Anzahl der Androiden
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		if(androidenAnzahl < 0) androidenAnzahl = 0;
		this.androidenAnzahl = androidenAnzahl;
	}
	/**
	 * Get der schiffsname
	 * 
	 * @return schiffsname 
	 */
	public String getSchiffsname() {
		return schiffsname;
	}
	/**
	 * Der schiffsname setzen
	 * 
	 * @param schiffsname Der Schiffsname
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	/**
	 * Get das Ladungsverzeichnis (ArrayList von Ladung-Klassen)
	 * 
	 * @return ladungverzeichnis
	 */
	public ArrayList<Ladung> getLadungverzeichnis() {
		return ladungverzeichnis;
	}
	/**
	 * Das Ladungsverzeichnis (ArrayList von Ladung-Klassen) setzen
	 * 
	 * @param ladungverzeichnis Das Ladungsverzeichnis
	 */
	public void setLadungverzeichnis(ArrayList<Ladung> ladungverzeichnis) {
		this.ladungverzeichnis = ladungverzeichnis;
	}
	
	/**
	 * Hinzuf�gen von Ladungsklassen im ArrayList
	 * 
	 * @param neueLadung eine neue "Ladung" Klasse 
	 */
	
	public void addLadung(Ladung neueLadung) {
		getLadungverzeichnis().add(neueLadung);
	}
	
	/**
	 * Zeigt den Zustand des Raumschiffs in der Konsole an
	 */
	public void zustandRaumschiff() {
		System.out.printf("=============== Zustand von %s ===============\n", getSchiffsname());
		System.out.println("Photonentorpedo: "+getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung: "+getEnergieversorgungInProzent()+"%");
		System.out.println("Schilde: "+getSchildeInProzent()+"%");
		System.out.println("H�lle: "+getHuelleInProzent()+"%");
		System.out.println("Androiden: "+getAndroidenAnzahl());
		System.out.println("Lebenserhaltungssysteme: "+getLebenserhaltungssystemeInProzent()+"%");
		System.out.println("=======================================================");
	}
	/**
	 * Zeigt die Werte der Ladeklassen vom ArrayList in der Konsole an
	 */
	
	public void ladungverzeichnisAusgeben() {
		if(getLadungverzeichnis().size() <= 0) {
			System.out.println("Das Ladungverzeichnis ist leer.");
			return;
			// Wenn die ArrayList-gr��e 0 ist, wird die Methode gestoppt
		}
		int count = 1;
		System.out.println("Ladungen von "+getSchiffsname()+":\n");
		for (Ladung ladung : getLadungverzeichnis()) {
			System.out.printf("Ladung%d\nBeschreibung: %s\nMenge: %d\n\n", count, ladung.getBezeichnung(), ladung.getMenge());
			count++;
			//Zeigt alle Werte in diesen Klassen in der Konsole an
		}
	}
	
	/**
	 * Ein Photonentorpedo auf ein Raumschiff Schie�en
	 * 
	 * @param r Ziel-Raumschiff
	 */
	public void photonentorpedosSchiessen(Raumschiff r) {
		int photonentorpedoAnzahl = getPhotonentorpedoAnzahl();
		if(photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
			return;
			// Wenn es keine Photonentorpedos gibt, wird die Methode gestoppt
		}
		
		nachrichtAnAlle("Photonentorpedos abgeschossen");
		
		setPhotonentorpedoAnzahl(photonentorpedoAnzahl-1); //  Anzahl der Photonentorpedos reduzieren
		treffer(r); 
	}

	/**
	 * Ein Phaserkanon auf ein Raumschiff Schie�en
	 * 
	 * @param r Ziel-Raumschiff
	 */
	public void phaserKanoneSchiessen(Raumschiff r) {
		int energieVersorgung = getEnergieversorgungInProzent();
		if(energieVersorgung < 50) {
			nachrichtAnAlle("-=*Click*=-");
			return;
			// Wenn die Energieversorgung kleiner als 50%, wird die Methode gestoppt
		}
		
		setEnergieversorgungInProzent(energieVersorgung - 50); // Energieversorgung um 50% reduzieren
		nachrichtAnAlle("Phaserkanone abgeschossen");
		treffer(r);
	}
	
	/**
	 * Eine Nachricht in der Konsole Anzeigen und sie im ArrayList hinzuf�gen
	 * 
	 * @param message Nachricht
	 */
	public void nachrichtAnAlle(String message) {
		
		Raumschiff.broadcastKommunikator.add(message);
		System.out.println(message);
	}
	
	/**
	 * Get ein ArrayList von Nachrichten (String)
	 * 
	 * @return broadcastKommunikator
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return Raumschiff.broadcastKommunikator;
	}
	
	/**
	 * Ein Raumschiff Schaden zuf�gen
	 * 
	 * @param r Ziel-Raumschiff
	 */
	private void treffer(Raumschiff r) {
		nachrichtAnAlle(r.getSchiffsname() + " wurde getroffen!");
		
		r.setSchildeInProzent(r.getSchildeInProzent() - 50); // Schile um 50% reduzieren
		
		if(r.getSchildeInProzent() == 0) { // nur wenn die Schilde bei 0% ist,
			int huelleProzent = r.getHuelleInProzent();
			if(huelleProzent == 0) return;
			// Wenn die H�lle bei 0% ist, wird die Methode gestoppt
			
			r.setHuelleInProzent(huelleProzent - 50); // H�lle um 50% reduzieren
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() - 50); // Energieversorgung um 50% reduzieren
			
			if(r.getHuelleInProzent() == 0) {
				// Wenn die H�lle nach der Reduzierung von 50% bei 0% ist
				
				r.setLebenserhaltungssystemeInProzent(0); //Lebenserhaltungssystem zerst�ren
				nachrichtAnAlle(String.format("Die Lebenserhaltungssysteme von %s wurden vernichtet!", r.getSchiffsname()));
			}
		}
	}
	/**
	 *<pre>Laden die photonentorpedos nur, wenn
	 * das Ladungverzeichnis die Ladung "Photonentorpedo" enth�lt.
	 * 
	 * @param anzahl Anzahl der photonentorpedos
	 * </pre>
	 */
	public void photonentorpedosLaden(int anzahl) {
		
		// Pr�fen, ob das Ladungverzeichnis die Ladung "Photonentorpedo" enth�lt. 	
		Ladung photonentorperdoLadung = null;
		for (Ladung ladung : getLadungverzeichnis()) {
			if(ladung.getBezeichnung().equals("Photonentorpedo")) {
				photonentorperdoLadung = ladung;
				break;
			}
		}
		
		if(photonentorperdoLadung != null) { //wenn das Ladungverzeichnis die Ladung "Photonentorpedo" enth�lt
			if(anzahl > photonentorperdoLadung.getMenge()) {
				anzahl = photonentorperdoLadung.getMenge(); 
				//Wenn der Wert des Parameters gr��er als die Menge der Ladung ist, wird der Parameter begrenzt
			}
				
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + anzahl); //photonentorpedo hinz�fugen
			photonentorperdoLadung.setMenge(photonentorperdoLadung.getMenge() - anzahl); 
			// Verringern die Ladung-Menge
			nachrichtAnAlle(anzahl + " Photonentorpedos eingesetzt");

			if(photonentorperdoLadung.getMenge() == 0) {
				ladungsverzeichnisAufraeumen();
				//wenn die Ladung-Menge 0 ist, dann wird es gel�scht
			}

		}
		else {
			nachrichtAnAlle("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
	}
	/**
	 * Entfernt die Ladungen aus einer Arrayliste mit einer Menge von 0 
	 */
	public void ladungsverzeichnisAufraeumen() {

		ArrayList<Ladung> gefiltertLadungen = new ArrayList<>();
		for (Ladung ladung : getLadungverzeichnis()) {
			if(ladung.getMenge() != 0) {
				gefiltertLadungen.add(ladung);
				//wenn die Ladung-Menge nicht 0 ist, dann wird es einer neuen Arrayliste hinzugef�gt
			}
		}
		
		setLadungverzeichnis(gefiltertLadungen); 
		//Ersetzen die alte Arrayliste durch die neue, die nur Ladung-Klassen mit einer Menge gr��er als 0 enth�lt
	}
	
	/**
	 *<pre>Teile des Raumschiffs reparieren
	 *Je mehr Androiden es gibt, desto h�her ist der Reparaturprozentsatz 
	 *Je mehr Parameter auf "true" gesetzt sind, desto niedriger ist der Prozentsatz 
	 * 
	 * @param schutzschilde soll die Schilde repariert werden?
	 * @param energieversorgung soll die Energieversorgung repariert werden? 
	 * @param schiffhuelle soll die H�lle repariert werden?
	 * @param anzahlDroiden wie viel Androiden, um das Schiff zu reparieren
	 * </pre>
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffhuelle, int anzahlDroiden) {
		if(anzahlDroiden > getAndroidenAnzahl()) {
			anzahlDroiden = getAndroidenAnzahl(); 
			//Begrenzen den Parameter auf die Anzahl der auf dem Schiff verf�gbaren Androiden
		}
		
		double anzahlTrue = 0;
		if(schutzschilde) anzahlTrue++;
		if(energieversorgung) anzahlTrue++;
		if(schiffhuelle) anzahlTrue++;
		//Z�hlen die Anzahl der boolean Werte, die auf "true" gesetzt sind
		
		double zufallszahl = new Random().nextInt(100)+1; //Eine Zufallszahl zwischen  1 und 100 erzeugen
		
		int prozent = (int)((zufallszahl * (double)anzahlDroiden) / anzahlTrue); // z.B: 87 * 2 / 3 = 58%
		
		if(schutzschilde) {
			setSchildeInProzent(getSchildeInProzent() + prozent);
		}
		if(energieversorgung) {
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() + prozent);
		}
		if(schiffhuelle) {
			setHuelleInProzent(getHuelleInProzent() + prozent);
		}
		/*Erh�hen den Prozentsatz dieser 3 Variablen, wenn die Parameter auf "true" stehen.
		Information: Die Setter Methoden begrenzen die Parameter auf 100%
		*/
	}

	
}
