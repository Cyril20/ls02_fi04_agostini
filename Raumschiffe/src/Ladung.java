/**
 * Klasse zum Erstellen und Verwalten von Ladung
 * 
 * @author Cyril Agostini
*/

public class Ladung {
	
	private String bezeichnung;
	private int menge;
	
	/**
	 * Erstellung einer neuen Instanz und Variablen einrichten
	*/
	public Ladung() {
		setBezeichnung("Keine");
		setMenge(0);
	}
	/**
	 * Erstellung einer neuen Instanz und Variablen einrichten
	 * 
	 * @param bezeichnung Bezeichnung der Ladung
	 * @param menge Menge der Ladung
	 */
	public Ladung(String bezeichnung, int menge) {
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}
	
	/**
	 * Get die Bezeichnung der Ladung
	 * 
	 * @return bezeichnung
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}
	/**
	 * Die Bezeichnung der Ladung setzen
	 * 
	 * @param bezeichnung Bezeichnung der Ladung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	/**
	 * Get die menge der Ladung 
	 * 
	 * @return menge
	 */
	public int getMenge() {
		return menge;
	}
	/**
	 * Die Menge der Ladung setzen
	 * @param menge Menge der Ladung
	 */
	public void setMenge(int menge) {
		if(menge < 0) menge = 0;
		this.menge = menge;
	}
	


}
