package Rechteck;

import java.awt.Point;

public class Rechteck {

	private double breite;
	private double lange;
	private Point position;

	public Rechteck(double breite, double lange, Point position) {
		setBreite(breite);
		setLange(lange);
		setPosition(position);
	}
	
	public double getBreite() {
		return breite;
	}

	public void setBreite(double breite) {
		this.breite = breite;
	}

	public double getLange() {
		return lange;
	}

	public void setLange(double lange) {
		this.lange = lange;
	}

	public double GetFlaeche(){

		return this.lange * this.breite;
	}

	public double getUmfang() {

		return (this.lange * 2) + (this.breite * 2);

	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}
}
